<?php
return [
    'settings' => [
        'displayErrorDetails' => true,
        'template' => 'bootstrap-template',
        'mail' => [
            'from' => 'cw.tw1nk@mail.ru',
            'from_name' => 'CW.Team',
            'username' => 'cw.tw1nk@mail.ru',
            'password' => '123321.a',
            'host' => 'smtp.mail.ru',
            'port' => 465,
        ],
        'db' => [
            'database' => 'web',
            'hostname' => 'mariadb',
            'user' => 'root',
            'password' => 'testpass'

        ],
        'hash_algo' => PASSWORD_BCRYPT,
    ],
];