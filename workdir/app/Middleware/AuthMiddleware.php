<?php


namespace App\Middleware;


use App\Exceptions\UserNotFoundException;
use App\Services\UserService;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class AuthMiddleware
{
    private $c;

    /**
     * Guest constructor.
     * @param ContainerInterface $c
     */
    public function __construct(ContainerInterface $c)
    {
        $this->c = $c;

    }
    public function __invoke(Request $request,Response $response, $next)
    {
        /** @var UserService $userService */
        $userService = $this->c->get('userService');
        if (isset($_COOKIE['user_id']) and isset($_COOKIE['token']) and !$userService->hasCurrentUser()){
            try {
                $userBySession = $userService->getUserBySession($_COOKIE['user_id'], $_COOKIE['token']);
                if($userBySession == null) return $response->withRedirect('/logout',302);
                $userService->setCurrentUser($userBySession);
                setcookie('user_id', $_COOKIE['user_id'], time()+60*60*24*30);
                setcookie('token', $_COOKIE['token'], time()+60*60*24*30);
            } catch (UserNotFoundException $e) {
                setcookie('user_id', 'null', time()-1);
                setcookie('token', 'null', time()-1);
                return $response->withRedirect('/login', 302);
            }
        }
        if($userService->hasCurrentUser()){
            return $next($request, $response);
        }
        return $response->withRedirect('/login', 302);
    }


}