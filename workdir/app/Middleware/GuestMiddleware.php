<?php


namespace App\Middleware;


use App\Services\GuestService;
use DateTime;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class GuestMiddleware
{

    private $c;

    /**
     * Guest constructor.
     * @param ContainerInterface $c
     */
    public function __construct(ContainerInterface $c)
    {
        $this->c = $c;

    }


    public function __invoke(Request $request,Response $response, $next)
    {
//        $request->getCookieParams();
        /** @var GuestService $guestService */
        $guestService = $this->c->get('guestService');
        if(!isset($_COOKIE['guest_id']) or !isset($_COOKIE['guest_sesId'])){
            $this->create($request, $guestService);
        }else{
            if ($guestService->hasSession($_COOKIE['guest_id'], $_COOKIE['guest_sesId'])){
                setcookie('guest_id',$_COOKIE['guest_id'], time()+60*60*24*30, '/');
                setcookie('guest_sesId',$_COOKIE['guest_sesId'], time()+60*60*24*30,"/");
            }else{
                $this->create($request, $guestService);
            }
        }
        return $next($request, $response);
    }

    private function create(Request $request,GuestService $guestService ){
        $dateTime = new DateTime();
        $origin = $request->getServerParam("HTTP_REFERER");
        $sesId = $guestService->createSession($request->getServerParam('HTTP_USER_AGENT'), $_SERVER['REMOTE_ADDR'], $dateTime->format('Y-m-d H:i:s'), isset($origin)?$origin:'null');
        setcookie('guest_id',$sesId->getId(), time()+60*60*24*30,"/");
        setcookie('guest_sesId',$sesId->getSession(), time()+60*60*24*30,"/");
    }

}