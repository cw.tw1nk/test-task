<?php


namespace App\Middleware;


use App\Models\User;
use Slim\Http\Request;
use Slim\Http\Response;

class GuestAccessMiddleware
{
    public function __invoke(Request $request,Response $response, $next)
    {
        if (isset($_SESSION['user'])){
            return $response->withRedirect('/', 302);
        }
        return $next($request, $response);
    }
}