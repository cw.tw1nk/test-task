<?php


namespace App\Middleware;


use Psr\Container\ContainerInterface;

class Middleware
{

    protected $c;


    /**
     * Middleware constructor.
     * @param ContainerInterface $c
     */
    public function __construct(ContainerInterface $c)
    {
        $this->c = $c;
    }

}