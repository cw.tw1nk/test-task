<?php


namespace App\Middleware;


use App\Models\User;
use App\Services\UserService;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class RoleAdminAccessMiddleware extends Middleware
{


    public function __invoke(Request $request, Response $response, $next)
    {
        $role = $this->c->get('userService')->getCurrentRole();
        if($role === 'admin'){
            return $next($request, $response);
        }else if ($role === 'user'){
            return $response->withRedirect('/', 302);
        }else{
            return $response->withRedirect('/login', 302);

        }
//
//        if (isset($_SESSION['user'])){
//            /** @var User $user */
//            $user = unserialize($_SESSION['user']);
//            if ($user->isPrivileged() == 1){
//                return $next($request, $response);
//            }
//        }
    }
}