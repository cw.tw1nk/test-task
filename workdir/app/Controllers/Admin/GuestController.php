<?php


namespace App\Controllers\Admin;


use App\Controllers\Controller;
use PDO;
use Slim\Http\Request;
use Slim\Http\Response;


class GuestController extends Controller
{
    public function index($request, $response, $args){
        return $this->view->render($response,'@admin/guests.twig');
    }

    public function guests(Request $request,Response $response, $args){
        $draw = $request->getParam('draw');
        $start = $request->getParam('start');
        $length = $request->getParam('length');
        /** @var PDO $db */
        $db = $this->container->get('db');
        $search = $request->getParam('search')['value'];
        $columns = $request->getQueryParam('columns');

        $orderBy = array();
        $allowColumns = ['id', 'user_agent', 'ip', 'time', 'referrer', 'user_id'];
        foreach ($request->getParam('order') as $item) {
            $column = $columns[$item['column']]['data'];
            if (in_array($column, $allowColumns)){
                array_push($orderBy, $column .' '.$item['dir']);
            }
        }
        $users = $db->prepare('select id, user_agent, ip, time, referrer, user_id from (select *, null as \'user_id\' from session where id not in(select s.id from session s, (select auth_session.id, user_id, session_id from auth_session inner join user u on auth_session.user_id = u.id) as auths where s.id = auths.session_id group by s.id, user_id) union select s.id, user_agent, ip, time, referrer,session , user_id from session s, (select auth_session.id, user_id, session_id from auth_session inner join user u on auth_session.user_id = u.id) as auths where s.id = auths.session_id group by s.id, user_id) as s where id like :search or user_agent like :search or ip like :search or time like :search or referrer like :search '.'order by '.implode( ", ", $orderBy ).' limit '.intval($start).','.intval($length).';');
        $users->execute(array(':search' => "%$search%"));
        $totalRows = $db->query('select COUNT(*) from session')->fetchColumn();
        return $response->withJson(['draw'=>$draw,'recordsTotal'=>$totalRows,'recordsFiltered'=>empty($search)?$totalRows:$users->rowCount(),'data'=>$users->fetchAll()]);
    }

}