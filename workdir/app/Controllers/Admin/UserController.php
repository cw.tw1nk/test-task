<?php


namespace App\Controllers\Admin;


use App\Controllers\Controller;
use App\Exceptions\UserNotFoundException;
use App\Models\User;
use App\Services\UserService;
use App\Twig\Validation;
use DateTime;
use PDO;
use Psr\Container\ContainerInterface;
use Respect\Validation\Rules\AllOf;
use Respect\Validation\Rules\Length;
use Respect\Validation\Rules\NoWhitespace;
use Respect\Validation\Rules\StringType;
use Respect\Validation\Validator as v;
use Slim\Http\Request;
use Slim\Http\Response;

class UserController extends Controller
{
    /** @var UserService $userService */
    private $userService;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->userService = $this->container->get('userService');
    }


    public function index(Request $request,Response $response, $args){
        return $this->view->render($response,'@admin/users.twig');
    }
    public function users(Request $request,Response $response, $args){
        /** @var PDO $db */
        $draw = $request->getParam('draw');
        $start = $request->getParam('start');
        $length = $request->getParam('length');
        $db = $this->container->get('db');
        $search = $request->getParam('search')['value'];
        $columns = $request->getQueryParam('columns');
        $orderBy = array();
        $allowColumns = ['id', 'email', 'privileged', 'active', 'create_at'];
        foreach ($request->getParam('order') as $item) {
            $column = $columns[$item['column']]['data'];
            if (in_array($column, $allowColumns)){
                array_push($orderBy, $column .' '.$item['dir']);
            }
        }
        $users = $db->prepare('select id, email, privileged, active, create_at from user where id = :id or email like :search or create_at like :search '.'order by '.implode( ", ", $orderBy ).' limit '.intval($start).','.intval($length).';');
        $users->execute(array(':search' => "%$search%", 'id'=>$search));
        $totalRows = $db->query('select COUNT(*) from user')->fetchColumn();
        return $response->withJson(['draw'=>$draw,'recordsTotal'=>$totalRows,'recordsFiltered'=>empty($search)?$totalRows:$users->rowCount(),'data'=>$users->fetchAll()]);
    }
    public function deleteUser(Request $request,Response $response, $args){
        $nameKey = $this->csrf->getTokenNameKey();
        $valueKey = $this->csrf->getTokenValueKey();
        $name = $request->getAttribute($nameKey);
        $value = $request->getAttribute($valueKey);

        $tokenArray = [
            $nameKey => $name,
            $valueKey => $value
        ];

        $id = $request->getQueryParam('id');
        if (intval($id)===1){
            return $response->withStatus(400, 'Cannot change first user!');
        }else if (intval($id)===intval($this->userService->getCurrentUser()->getId())){
            return $response->withStatus(400, 'Cannot change current user!');
        }
        try {
            $this->userService->delete($id);
        } catch (UserNotFoundException $e) {

        }
        return $response->withJson(['result'=>'ok', 'csrf'=>$tokenArray],200);
    }



    public function createUser(Request $request, Response $response, array $args){
        /** @var Validation $validation */
        $validation = $this->container->get('validation');
        $validation->validate($request, [
            'email' => v::email(),
            'password' => v::allOf(
                new StringType(),
                new NoWhitespace(),
                new Length(6, 32)
            ),
            'privileged' => v::boolVal(),
            'active' => v::boolVal(),
        ], []);
        if (!$validation->hasErrors()) {
            $date = new DateTime;
            $email = $request->getParam('email');
            $password = $request->getParam('password');
            $privileged = self::toBool($request->getParam('privileged'));
            $active = self::toBool($request->getParam('active'));
//        $create_at = $request->getParam('create_at');
            $create_at = $date->format('Y-m-d H:i:s');
            try {
                $this->userService->create($email, $password, $privileged, $active, $create_at);
                return $response->withStatus(201, 'Created');
            } catch (\PDOException $e) {
                if ($e->errorInfo[1] == 1062) {
                    return $response->withStatus(409, 'Already exist');
                }
            }
            return $response->withStatus(400);
        }
        return $response->withStatus(400, 'Request contains invalid values');
    }

    public function updateUser(Request $request, Response $response, array $args){
        /** @var Validation $validation */
        $validation = $this->container->get('validation');
        $allOf = new AllOf();
        $validation->validate($request, [
            'email' => v::email(),
            'password' => v::optional(v::allOf(
                new StringType(),
                new NoWhitespace(),
                new Length(6, 32)
            )),
            'privileged' => v::boolVal(),
            'active' => v::boolVal(),
        ], []);
        if (!$validation->hasErrors()) {
            $user_id = $args['id'];
            if(intval($user_id) === 1)
                return $response->withStatus(400, 'Cannot change first user!');
            if (!$this->userService->hasUser($user_id))
                return $response->withStatus(404, 'User not found');
            $email = $request->getParam('email');
            $password = $request->getParam('password');
            $privileged = self::toBool($request->getParam('privileged'));
            $active = self::toBool($request->getParam('active'));
            $create_at = $request->getParam('create_at');
            $this->userService->update($user_id, $email, $password, $privileged, $active, $create_at);
            return $response->withStatus(204, 'Updated');
        }
        return $response->withStatus(400, 'Request contains invalid values');
    }

    /**
     * @param string $value
     * @return bool
     */
    private static function toBool(string $value){
        switch ($value){
            case '1':
            case 'true':
            case 'on':
                return true;
            default: return false;
        }
    }
}
/*
 * http://localhost/admin/api/users?draw=1
 * &columns[0][data]=id
 * &columns[0][name]=
 * &columns[0][searchable]=true
 * &columns[0][orderable]=true
 * &columns[0][search][value]=
 * &columns[0][search][regex]=false
 * &columns[1][data]=email
 * &columns[1][name]=
 * &columns[1][searchable]=true
 * &columns[1][orderable]=true
 * &columns[1][search][value]=
 * &columns[1][search][regex]=false
 * &columns[2][data]=privileged
 * &columns[2][name]=
 * &columns[2][searchable]=true
 * &columns[2][orderable]=true
 * &columns[2][search][value]=
 * &columns[2][search][regex]=false
 * &columns[3][data]=active
 * &columns[3][name]=
 * &columns[3][searchable]=true
 * &columns[3][orderable]=true
 * &columns[3][search][value]=
 * &columns[3][search][regex]=false
 * &columns[4][data]=create_at
 * &columns[4][name]=
 * &columns[4][searchable]=true
 * &columns[4][orderable]=true
 * &columns[4][search][value]=
 * &columns[4][search][regex]=false
 * &order[0][column]=0
 * &order[0][dir]=asc
 * &start=0&length=10
 * &search[value]=
 * &search[regex]=false
 * &_=1559637304768
 */