<?php


namespace App\Controllers;


use App\Exceptions\PasswordException;
use App\Exceptions\UserNotFoundException;
use App\Services\UserService;
use App\Twig\Validation;
use Respect\Validation\Rules\Length;
use Respect\Validation\Rules\NoWhitespace;
use Respect\Validation\Rules\StringType;
use Respect\Validation\Validator as v;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

class ProfileController extends Controller
{

    public function index(Request $request,Response $response, $args){
        $msg = [];
        if ($this->flash->hasMessage('success')){
            $msg['success_msg'] = $this->flash->getFirstMessage('success');
        }
        return $this->view->render($response, 'profile.twig', $msg);
    }

    public function update(Request $request,Response $response, $args){
        /** @var UserService $userService */
        $userService = $this->container['userService'];
        /** @var Validation $validation */
        $validation = $this->container->get('validation');
        $oldPassword = $request->getParam('old_password');
        $newPassword = $request->getParam('new_password');

        $validation->validate($request, [
            'old_password' => v::allOf(
                new StringType(),
                new NoWhitespace(),
                new Length(6, 32)
            ),
            'new_password' => v::allOf(
                new StringType(),
                new NoWhitespace(),
                new Length(6, 32)
            ),
            'password_repeat' => v::equals($newPassword)
        ], [
            'old_password' => 'Old password is not equal to current',
            'new_password' => 'Password must contain 6 characters, must not contain spaces.',
            'password_repeat' => 'Passwords are not equal',
        ]);
        if (!$validation->hasErrors()){
            try {
                $userService->changeUserPassword($userService->getCurrentUser(), $oldPassword, $newPassword);
                $this->flash->addMessage('success', 'Password has been successfully changed');
                return $response->withRedirect($this->router->pathFor('profile'), 302);
            } catch (UserNotFoundException $e) {
                $validation->addError('user','User error');
            } catch (PasswordException $e) {
                $validation->addError('old_password', 'Old password is not equal to current');
            }
        }
        return $this->view->render($response, 'profile.twig');
    }

}