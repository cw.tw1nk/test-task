<?php


namespace App\Controllers;

use Psr\Container\ContainerInterface;
use Slim\Flash\Messages;
use Slim\Router;
use Slim\Views\Twig;

/**
 * @property Messages flash
 * @property Twig view
 * @property Router router
 */
class Controller
{

    /** @var ContainerInterface $container */
    protected $container;

    /**
     * Controller constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    /**
     * @param $param
     * @return Object|null
     */
    public function __get($param){
        if($this->container->{$param}){
            return $this->container->{$param};
        }
        return null;
    }
}