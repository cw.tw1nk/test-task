<?php


namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\Uri;

class TestController extends Controller
{
    public function index(Request $request,Response $response, $args){
//        if (isset($_COOKIE['user_id']) and isset($_COOKIE['token'])){
//            if (isset($_SESSION['user'])){
//                return $response->withJson(unserialize($_SESSION['user']));
//            }
//            return '<br/>';
//        }
//        return 'error';
        $basePath = Uri::createFromEnvironment($this->container->get('environment'))->getBaseUrl();

        return $this->view->fetch('@mail/verification.twig', ['url' => $basePath . $this->router->pathFor('confirm.get', ['email' => 'email@email.com'], ['key' => 'this_is_key'])]);
    }
}