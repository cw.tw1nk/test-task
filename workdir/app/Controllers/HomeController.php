<?php /** @noinspection PhpUndefinedFieldInspection */


namespace App\Controllers;


class HomeController extends Controller
{

    /**
     * @param $request
     * @param $response
     * @param $args
     * @return mixed
     */
    public function index($request, $response, $args){
        $arr = [
            'session' => session_id(),
        ];
        if ($this->flash->hasMessage('error'))
            $arr['error_msg'] = $this->flash->getFirstMessage('error');
        if ($this->flash->hasMessage('success')){
            $arr['success_msg'] = $this->flash->getFirstMessage('success');
            $arr['success_title'] = $this->flash->getFirstMessage('success_title', 'Success');
        }
        $this->flash->clearMessages();
        return $this->view->render($response, 'home.twig', $arr);
    }
}