<?php


namespace App\Controllers\Auth;


use App\Controllers\Controller;
use Slim\Http\Request;
use Slim\Http\Response;

class LogoutController extends Controller
{
    public function logout(Request $request, Response $response, $args)
    {
        $this->container->get('userService')->logout();
        return $response->withRedirect('/');
    }

}