<?php


namespace App\Controllers\Auth;


use App\Controllers\Controller;
use App\Exceptions\EmailConfirmationException;
use App\Exceptions\UserNotFoundException;
use App\Services\UserService;
use Respect\Validation\Validator as v;
use Slim\Flash\Messages;
use Slim\Http\Request;
use Slim\Http\Response;


class EmailVerificationController extends Controller
{
    public function confirm(Request $request, Response $response, $args){

        $key = $request->getParam('key');
        $email = $args['email'];
        if (!v::email()->validate($email)){
            return 'Email error format';
        }
        try{
            /** @var UserService $userService */
            $userService = $this->container['userService'];
            $userService->verifyEmail($email,$key);
            $this->flash->addMessage('success', 'You have been successfully registered.');
            return $response->withRedirect('/', 302);
        } catch (EmailConfirmationException $e) {
            $this->flash->addMessage('error', 'Email verification error');
        } catch (UserNotFoundException $e) {
            $this->flash->addMessage('error', 'Email verification error');
        }
        return $response->withRedirect('/', 302);
    }
}