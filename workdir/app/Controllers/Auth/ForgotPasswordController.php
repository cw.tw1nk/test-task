<?php


namespace App\Controllers\Auth;


use App\Controllers\Controller;
use App\Exceptions\TokenNotFoundException;
use App\Exceptions\UserNotFoundException;
use App\Services\UserService;
use App\Twig\Validation;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Respect\Validation\Exceptions\ComponentException;
use Respect\Validation\Rules\Length;
use Respect\Validation\Rules\NoWhitespace;
use Respect\Validation\Rules\StringType;
use Respect\Validation\Validator as v;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

class ForgotPasswordController extends Controller
{
    /** @var UserService */
    private $userService;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->userService = $container['userService'];
    }


    public function index(Request $request,Response $response, $args){
        return $this->view->render($response,'forgot.twig');
    }

    public function forgot(Request $request,Response $response, $args){
        /** @var Validation $validation */
        $validation = $this->container->get('validation');
        $validation->validate($request, [
            'email' => v::email(),
        ], [
            'email' => 'Email not found',
        ]);
        if (!$validation->hasErrors()){
            $email = $request->getParam('email');
            try {
                $this->userService->applyRequestPasswordReset($email);
            } catch (UserNotFoundException $e) {
                $validation->addError('email','Email not found.' );
                return $this->view->render($response,'forgot.twig');
            }
            $this->flash->addMessage('success', 'We sent instructions for changing the password to '.$email.', please check your Inbox and Spam folders.');
            return $response->withRedirect('/', 302);
        }
        return $this->view->render($response,'forgot.twig');
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return ResponseInterface|Response
     * @throws ComponentException
     * @throws UserNotFoundException
     */
    public function resetPassword(Request $request, Response $response, $args){
        /** @var Validation $validation */
        $validation = $this->container->get('validation');
        $validation->validate($request, [
            'email' => v::email(),
            'token' => v::allOf(
                new StringType(),
                new NoWhitespace(),
                new Length(32,32)
            ),
        ], [
            'email' => 'Email invalid',
            'token' => 'Token invalid'
        ]);
        if (!$validation->hasErrors()){
            $token = $request->getQueryParam('token');
            $email = $request->getQueryParam('email');
            if ($request->isGet()){
                if ($this->userService->checkTokenResetPassword($email,$token))
                    return $this->view->render($response,'reset-password.twig', ['token'=>$token, 'email'=>$email]);
                return $response->withRedirect('/', 302);
            }else if ($request->isPost()){
                $password = $request->getParam('password');
                $validation->validate($request, [
                    'password' => v::allOf(
                        new StringType(),
                        new NoWhitespace(),
                        new Length(6,32)
                    ),
                    'password_repeat'=> v::equals($password),
                    ], [
                        'password' => 'Password must contain 6 characters, must not contain spaces.',
                        'password_repeat' => 'Passwords are not equal',
                ]);

                if(!$validation->hasErrors()){
                    try {
                        $this->userService->changePassword($email, $token, $password);
                        $this->flash->addMessage('success', 'Password has been successfully changed.');
                        return $response->withRedirect('/', 302);
                    } catch (TokenNotFoundException $e) {
                        $this->flash->addMessage('error', 'Incorrect email or token.');
                        return $response->withRedirect('/', 302);
                    }
                }
                return $this->view->render($response,'reset-password.twig', ['token'=>$token, 'email'=>$email]);
            }
        }
        $this->flash->addMessage('error', 'Incorrect email or token.');
        return $response->withRedirect('/', 302);

    }

}