<?php


namespace App\Controllers\Auth;


use App\Controllers\Controller;

use App\Exceptions\UserNotFoundException;
use App\Models\AuthSession;
use App\Models\User;
use App\Services\UserService;
use App\Twig\Validation;
use Respect\Validation\Rules\Length;
use Respect\Validation\Rules\NoWhitespace;
use Respect\Validation\Rules\StringType;
use Respect\Validation\Validator as v;
use Slim\Http\Request;
use Slim\Http\Response;


class LoginController extends Controller
{

    public function login(Request $request, Response $response, $args){
        return $this->view->render($response ,'login.twig');
    }

    public function auth(Request $request, Response $response, $args){
        /** @var Validation  $validation */
        $validation = $this->container->get('validation');
        $validation->validate($request, [
            'email' => v::email(),
            'password' => v::allOf(
                new StringType(),
                new NoWhitespace(),
                new Length(6,32)
            )
        ], [
            'email' => 'Email entered invalid',
            'password' => 'Password invalid',
        ]);
        try{
            /** @var UserService $userService */
            $userService = $this->container->get('userService');
            /** @var User $user */
            $user = $userService->login($request->getParam('email'), $request->getParam('password'));
            /** @var AuthSession $session */
            $session = $userService->createSession($user->getId(), $_COOKIE['guest_id']);
            setcookie('token', $session->getToken(),time()+60*60*24*30,'/','',false, true);
            setcookie('user_id',$user->getId(),time()+60*60*24*30, '/','',false, true);
            $_SESSION['user'] = serialize($user);


            return $response->withRedirect('/',302);

        }catch (UserNotFoundException $e){
            $validation->addError('auth', 'Incorrect email or password');
            return $this->view->render($response, 'login.twig');
        }
    }

}