<?php


namespace App\Controllers\Auth;

use App\Controllers\Controller;


use App\Twig\Validation;
use Respect\Validation\Rules\Length;
use Respect\Validation\Rules\NoWhitespace;
use Respect\Validation\Rules\StringType;
use Respect\Validation\Validator as v;
use Slim\Http\Request;
use Slim\Http\Response;

class RegisterController extends Controller
{

    public function register(Request $request, Response $response, $args)
    {
        return $this->view->render($response, 'register.twig');
    }

    public function registerApi(Request $request, Response $response, $args)
    {
        $email = $request->getParam('email');
        $password = $request->getParam('password');
        /** @var Validation $validation */
        $validation = $this->container->get('validation');

        $validation->validate($request, [
            'email' => v::email(),
            'password' => v::allOf(
                new StringType(),
                new NoWhitespace(),
                new Length(6, 32)
            ),
            'password_repeat' => v::equals($password)
        ], [
            'email' => 'Email entered invalid',
            'password' => 'Password must contain 6 characters, must not contain spaces.',
            'password_repeat' => 'Passwords not equals',
        ]);
        if (!$validation->hasErrors()) {
            if ($this->container->get('UserDAO')->hasEmail($email)) {
                $validation->addError('email', 'This email is already registered.', $email);
            } else {
                $this->container->get('userService')->registerUser($email, $password);
                $this->flash->addMessage('success_title', 'Now you must activate your account!');
                $this->flash->addMessage('success', 'Follow the link we just sent you. If you do not find the letter in the mail, check Spam.');
                return $response->withRedirect('/', 302);
            }
        }
        return $this->view->render($response, 'register.twig');
    }


}