<?php


namespace App\Services;


use PHPMailer\PHPMailer\PHPMailer;
use Psr\Container\ContainerInterface;

class MailService extends Service
{
    private $from;
    private $fromName;
    private $username;
    private $password;
    private $host;
    private $port;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $mailConfig = $this->container['settings']['mail'];
        $this->from = $mailConfig['from'];
        $this->fromName = $mailConfig['from_name'];
        $this->username = $mailConfig['username'];
        $this->password = $mailConfig['password'];
        $this->host = $mailConfig['host'];
        $this->port = $mailConfig['port'];
    }


    public function sendMail($address, $title, $html, $plainText){


        $mail = new PHPMailer;
        $mail->SMTPDebug = 0;
        $mail->isSMTP();
        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mail->SMTPSecure = 'ssl';                                 // Enable TLS encryption, `ssl` also accepted
        $mail->Host       = $this->host;  // Specify main and backup SMTP servers
        $mail->Username   = $this->username;                     // SMTP username
        $mail->Password   = $this->password;                               // SMTP password
        $mail->Port       = $this->port;
        $mail->setFrom($this->from, $this->fromName);
        $mail->addAddress($address);     // Add a recipient

        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $title;
        $mail->Body    = $html;
        $mail->AltBody = $plainText;
        $mail->send();
    }

}