<?php


namespace App\Services;


use App\DAO\AuthSessionDao;
use App\DAO\EmailConfirmationDAO;
use App\DAO\ResetPasswordDao;
use App\DAO\UserDAO;
use App\Exceptions\PasswordException;
use App\Exceptions\TokenNotFoundException;
use App\Exceptions\UserNotFoundException;
use App\Models\AuthSession;
use App\Models\EmailConfirmation;
use App\Models\ResetPassword;
use App\Models\Session;
use App\Models\User;
use DateTime;
use Exception;
use Psr\Container\ContainerInterface;
use Slim\Http\Uri;
use Slim\Router;

class UserService extends Service
{
    /** @var UserDAO */
    private $userDao;
    /** @var EmailConfirmationDAO */
    private $emailConfirmationDao;
    /** @var AuthSessionDao */
    private $authSessionDao;
    /** @var ResetPasswordDao */
    private $resetPasswordDao;

    private $hash_algo;
    /**
     * UserService constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->userDao = $container['UserDAO'];
        $this->emailConfirmationDao = $this->container->get('EmailConfirmationDAO');
        $this->authSessionDao = $this->container->get('authSessionDAO');
        $this->resetPasswordDao = $this->container->get('resetPasswordDAO');
        $this->hash_algo = $container['settings']['hash_algo'];
    }


    /**
     * @param $email
     * @param $password
     * @throws Exception
     */
    public function registerUser($email, $password)
    {
        $date = new DateTime;
        $user = new User();
        $user->setEmail($email);
        $user->setPrivileged(0);
        $user->setActive(0);
        $user->setCreateAt($date->format('Y-m-d H:i:s'));
        $user->setPassword(password_hash($password,$this->hash_algo));
//        $user->setPassword(md5(md5($password)));
        /** @var User $user */
        $user = $this->container->get('UserDAO')->save($user);
        $emailConfirmation = new EmailConfirmation();
        $emailConfirmation->setUserId($user->getId());
        $emailConfirmation->setHash(md5($email.date('hdmY')));
        $this->emailConfirmationDao->save($emailConfirmation);
        $mailService = $this->container->get('mailService');

        $basePath = Uri::createFromEnvironment($this->container->get('environment'))->getBaseUrl();
        /** @var Router $router */
        $router = $this->container->get('router');
        $relativePath = $router->pathFor('confirm.get',['email' => $email], ['key'=>$emailConfirmation->getHash()]);
        $mailVerifyTemplate = $this->container->get('view')->fetch('@mail/verification.twig', ['url'=>$basePath.$relativePath]);
//        $mailVerifyTemplate = strtr($this->container['settings']['mailVerifyTemplate'], array("{%uri%}" => $basePath.$relativePath));
        $mailService->sendMail($email, 'Verify email', $mailVerifyTemplate, 'Text');
    }

    /**
     * @param $email
     * @param $key
     * @throws UserNotFoundException
     */
    public function verifyEmail($email, $key){
        $emailConfirmationDao = $this->container->get('EmailConfirmationDAO');
        $confirmation = $emailConfirmationDao->findByEmailAndHash($email, $key);
        $user = $this->userDao->findByEmail($email);
        $user->setActive(1);
        $this->userDao->save($user);
        $emailConfirmationDao->delete($confirmation);
    }

    /**
     * @param $email
     * @param $password
     * @return User
     * @throws UserNotFoundException
     */
    public function login($email, $password):User {
        $user = $this->userDao->findByEmail($email);
        if($user->isActive()){
            if (password_verify($password, $user->getPassword()))
                return $user;
        }
        throw new UserNotFoundException();
    }

    /**
     * @param $user_id
     * @param $session_id
     * @return AuthSession
     */
    public function createSession($user_id, $session_id){
        $randStr = substr(str_shuffle('qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890$'),0, 32);
        $authSession = new AuthSession();
        $authSession->setToken($randStr);
        $authSession->setUserId($user_id);
        $authSession->setSessionId($session_id);
        return $this->authSessionDao->save($authSession);
    }

    /**
     * @param $token
     */
    public function destroySession($token){
        $this->authSessionDao->deleteByToken($token);
    }

    /**
     * @param $user_id
     * @param $token
     * @return User|null
     * @throws UserNotFoundException
     */
    public function getUserBySession($user_id, $token){
        if($this->authSessionDao->hasByUserAndToken($user_id, $token)){
            return $this->userDao->findById($user_id);
        }
        throw new UserNotFoundException();
    }

    /**
     * @param $id
     * @throws UserNotFoundException
     */
    public function delete($id){
        $this->userDao->delete($this->userDao->findById($id));
    }

    /**
     * @param string $email
     * @param string $token
     * @param string $newPassword
     * @throws TokenNotFoundException
     * @throws UserNotFoundException
     */
    public function changePassword(string $email,string $token,string $newPassword){
        $this->resetPasswordDao->findByEmailAndToken($email, $token);
        $user = $this->userDao->findByEmail($email);
        $user->setActive(1);
        $user->setPassword(password_hash($newPassword,$this->hash_algo));
        $this->userDao->save($user);
        $this->resetPasswordDao->deleteByUser($user->getId());
        $this->authSessionDao->deleteByUser($user->getId());
    }

    /**
     * @param string $email
     * @throws UserNotFoundException
     */
    public function applyRequestPasswordReset(string $email){
        $randStr = substr(str_shuffle('qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890$'),0, 32);
        /** @var MailService $mailService */
        $mailService = $this->container->get('mailService');
        $user = $this->userDao->findByEmail($email);

        $resetModel = new ResetPassword();
        $resetModel->setUserId($user->getId());
        $resetModel->setToken($randStr);

        $this->resetPasswordDao->save($resetModel);
        /** @var Router $router */
        $router = $this->container->get('router');
        $basePath = Uri::createFromEnvironment($this->container->get('environment'))->getBaseUrl();
        $relativePath = $router->pathFor('reset.get',[], ['token'=>$randStr,'email' => $email]);
//        $mailResetPasswordTemplate = strtr($this->container['settings']['mailResetPasswordTemplate'], array("{%uri%}" => $basePath.'/confirm/?email=' . $email . '&token=' . $randStr));
        $mailResetPasswordTemplate = $this->container->get('view')->fetch('@mail/reset.twig', ['url'=>$basePath.$relativePath]);
        $mailService->sendMail($email, 'Change password', $mailResetPasswordTemplate, 'Text');
    }


    /**
     * @param string $email
     * @param string $token
     * @return bool
     */
    public function checkTokenResetPassword(string $email, string $token){
       return $this->resetPasswordDao->hasByEmailAndHash($email, $token);
    }

    /**
     * @param User $user
     * @param string $oldPassword
     * @param string $newPassword
     * @throws PasswordException
     * @throws UserNotFoundException
     */
    public function changeUserPassword(User $user,string $oldPassword,string $newPassword){
        $user = $this->userDao->findById($user->getId());
        if(password_verify($oldPassword, $user->getPassword())){
            $user->setPassword(password_hash($newPassword,PASSWORD_BCRYPT));
            $this->userDao->save($user);
            $this->setCurrentUser($user);
            return;
        }
        throw new PasswordException('Old password is not equal to current');
    }

    /**
     * @return User|null
     */
    public function getCurrentUser(){
        if (isset($_SESSION['user'])){
            return unserialize($_SESSION['user']);
        }
        return null;
    }

    public function hasCurrentUser(){{
        return isset($_SESSION['user']);
    }}

    /**
     * @param User $user
     */
    public function setCurrentUser(User $user){
        $_SESSION['user'] = serialize($user);
    }

    public function logout(){
        if (isset($_SESSION['user'])) {
            if (isset($_COOKIE['token'])){
                $this->container->get('userService')->destroySession($_COOKIE['token']);
                setcookie('user_id', "null", time() - 1);
                setcookie('token', "null", time() - 1);
                unset($_COOKIE['user_id']);
                unset($_COOKIE['token']);
            }
            unset($_SESSION['user']);
            session_destroy();
        }
    }

    public function create(string $email, string $password, bool $privileged, bool $active, $createAt){
        $userEntity = new User();
        $userEntity->setEmail($email);
        $userEntity->setPassword(password_hash($password,$this->hash_algo));
        $userEntity->setPrivileged($privileged);
        $userEntity->setActive($active);
        $userEntity->setCreateAt($createAt);
        return $this->userDao->save($userEntity);
    }

    public function update(int $user_id, string $email, string $password, bool $privileged, bool $active, $createAt){
        $userEntity = new User();
        $userEntity->setId($user_id);
        $userEntity->setEmail($email);
        if(empty($password) or $password===''){
            $userEntity->setPassword($this->userDao->getPasswordById($user_id));
        }else{
            $userEntity->setPassword(password_hash($password,$this->hash_algo));
        }
        $userEntity->setPrivileged($privileged);
        $userEntity->setActive($active);
        $userEntity->setCreateAt($createAt);
        return $this->userDao->save($userEntity);
    }

    public function hasUser($user_id){
        return $this->userDao->hasId($user_id);
    }

    public function getCurrentRole(){
        if (isset($_SESSION['user'])){
            /** @var User $user */
            $user = unserialize($_SESSION['user']);
            $role = $this->userDao->getRoleById($user->getId());
            if (intval($role) === 1) return 'admin';
            return 'user';
        }
        return 'guest';
    }
}