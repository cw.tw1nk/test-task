<?php


namespace App\Services;


use Psr\Container\ContainerInterface;

class Service
{
    protected $container;

    /**
     * Service constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

}