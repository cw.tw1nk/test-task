<?php


namespace App\Services;


use App\DAO\SessionDao;
use App\Models\Session;
use function mysql_xdevapi\getSession;
use Psr\Container\ContainerInterface;

class GuestService extends Service
{
    /** @var SessionDao */
    private $sessionDao;
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->sessionDao = $container->get('sessionDao');
    }

    public function createSession($userAgent, $ip, $time, $referrer){
        $session = new Session();
        $session->setUserAgent($userAgent);
        $session->setIp($ip);
        $session->setTime($time);
        $session->setReferrer($referrer);
        $session->setSession(session_id());
        /** @var Session $session */
        $session = $this->sessionDao->save($session);
        return $session;
    }

    /**
     * @param int $user_id
     * @param string $session
     * @return bool
     */
    public function hasSession(int $user_id, string $session){
        return $this->sessionDao->hasByIdAndSession($user_id, $session);
    }
}