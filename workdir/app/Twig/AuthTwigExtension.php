<?php


namespace App\Twig;


use App\Models\User;
use Slim\Views\TwigExtension;
use Twig\Extension\GlobalsInterface;
use Twig\TwigFunction;

class AuthTwigExtension extends TwigExtension implements GlobalsInterface
{
    public function getFunctions()
    {
        return [
            new TwigFunction('is_auth', [$this, 'isAuth']),
            new TwigFunction('has_role', [$this, 'hasRole']),
            new TwigFunction('is_guest', [$this, 'isGuest']),
            new TwigFunction('is_user', [$this, 'isUser']),
            new TwigFunction('is_admin', [$this, 'isAdmin']),
        ];
    }

    public function isAuth(){
        return isset($_SESSION['user']);
    }

    public function isGuest(){
        return $this->hasRole('guest');
    }

    public function isUser(){
        return $this->hasRole('user');
    }

    public function isAdmin(){
        return $this->hasRole('admin');
    }

    public function hasRole($name){
        if (isset($_SESSION['user'])){
            /** @var User $user */
            $user = unserialize($_SESSION['user']);
            if ($name == 'admin' && $user->isPrivileged() == 1) return true;
            if ($name == 'user' && $user->isPrivileged() == 0) return true;
            return false;
        }else if ($name == 'guest'){
            return true;
        } else return false;
    }


    /**
     * Returns a list of global variables to add to the existing list.
     *
     * @return array An array of global variables
     */
    public function getGlobals()
    {
        $username = 'username';
        if (isset($_SESSION['user'])){
            /** @var User $user */
            $user = unserialize($_SESSION['user']);
            $username = $user->getEmail();
        }
        return ['username' => $username];
    }
}