<?php


namespace App\Twig;


use Slim\Csrf\Guard;
use Slim\Views\TwigExtension;
use Twig\Extension\GlobalsInterface;

class CsrfTwigExtension extends TwigExtension implements GlobalsInterface
{


    /**
     * @var Guard
     */
    protected $csrf;


    /**
     * @var Guard
     */
    public function __construct(Guard $csrf)
    {
        $this->csrf = $csrf;
    }
    /**
     * Returns a list of global variables to add to the existing list.
     *
     * @return array An array of global variables
     */
    public function getGlobals()
    {
        $csrfNameKey = $this->csrf->getTokenNameKey();
        $csrfValueKey = $this->csrf->getTokenValueKey();
        $csrfName = $this->csrf->getTokenName();
        $csrfValue = $this->csrf->getTokenValue();

        return [
            'csrf'   => [
                'keys' => [
                    'name'  => $csrfNameKey,
                    'value' => $csrfValueKey
                ],
                'name'  => $csrfName,
                'value' => $csrfValue
            ]
        ];
    }
    public function getName()
    {
        return 'slim/csrf';
    }
}