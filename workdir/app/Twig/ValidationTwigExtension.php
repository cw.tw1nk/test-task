<?php


namespace App\Twig;


use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ValidationTwigExtension extends AbstractExtension
{

    private $validation;

    /**
     * ValidationTwigExtension constructor.
     * @param Validation $validation
     */
    public function __construct(Validation $validation)
    {
        $this->validation = $validation;
    }


    public function getFunctions()
    {
        return [
            new TwigFunction('has_errors', [$this, 'hasErrors']),
            new TwigFunction('has_error', [$this, 'hasError']),
            new TwigFunction('get_error', [$this, 'getError']),
            new TwigFunction('error_value', [$this, 'getValue']),
        ];
    }

    public function hasErrors(){
        return $this->validation->hasErrors();
    }

    public function hasError($key){
        return $this->validation->hasError($key);
//        return !empty($this->validation->getError($key));
    }

    public function getError($key){
        return $this->validation->getError($key);
    }

    public function getValue($key){
        return $this->validation->getValue($key);
    }

}