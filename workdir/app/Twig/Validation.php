<?php


namespace App\Twig;

use Psr\Http\Message\ServerRequestInterface;
use Respect\Validation\Validatable;
use Respect\Validation\Validator as v;
use Slim\Http\Request;


class Validation
{
    private $errors = [];
    private $values = [];

    /**
     * Validation constructor.
     */
    public function __construct()
    {
//        if(isset($_SESSION)){
//            $this->errors = &$_SESSION['errors'];
//            $this->values = &$_SESSION['values'];
//        }
    }


    public function validate(Request $request,array $rules,array $messages = []){
        foreach ($rules as $param => $rule){
            $this->validateInput($this->getRequestParam($request, $param),$param,$rule,$messages);
        }
    }
    public function validateArray(array $array, array $rules,array $messages){
        foreach ($rules as $param => $rule){
            $this->validateInput($array[$param],$param,$rule,$messages);
        }
    }

    public function validateInput($data, $param, Validatable $rule, array $mesages){
        if (!$rule->validate($data)){
            $this->addError($param, $mesages[$param],$data);
        }
    }

    public function addError($key, $msg='', $data = null){
        if (!empty($key)) {
            $this->errors[$key] = $msg;
            $this->values[$key] = $data;
        }
    }

    public function getError($key){
        if (isset($this->errors[$key])) {
            $var = $this->errors[$key];
            unset($this->errors[$key]);
            return $var;
        }
        return '';
    }

    public function hasError($key){
        return isset($this->errors[$key]);
    }

    public function hasErrors(){
        return !empty($this->errors);
    }

    public function getValue($key){
        $var = $this->values[$key];
        unset($this->values[$key]);
        return $var;
    }

    private function getRequestParam(Request $request, $param)
    {
        $get = $request->getQueryParams();
        $post = $request->getParsedBody();
        if (is_array($post)&&isset($post[$param]))
            return $post[$param];
        else if (is_object($post) && property_exists($post, $param)){
            return $post->$param;
        }
        else if (isset($get[$param]))
            return $get[$param];
        else return '';
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

}