<?php



namespace App\DAO;



use App\Exceptions\UserNotFoundException;
use App\Models\User;
use PDO;
use Respect\Validation\Exceptions\ObjectTypeException;

class UserDAO extends BaseDAO implements DAO
{

    /**
     * @param User $object
     * @return User
     */
    public function save($object)
    {
        if($object instanceof User){
            $id = $object->getId();
            if (isset($id)){
                $prepare = $this->connection->prepare('select * from user where id=?');
                $prepare->execute(array($id));
                if ($prepare->rowCount()==1) {
                    $prepare = $this->connection->prepare('update user set email=?, password=?, privileged=?, active=?, create_at=? where id = ?');
                    $prepare->execute(array($object->getEmail(), $object->getPassword(), $object->isPrivileged()?1:0, $object->isActive()?1:0, $object->getCreateAt(), $id));
                    return $object;
                }
            }
            $prepare = $this->connection->prepare('insert into user set email=?, password=?, privileged=?, active=?, create_at=?');
            $prepare->execute(array($object->getEmail(), $object->getPassword(), $object->isPrivileged()?1:0, $object->isActive()?1:0, $object->getCreateAt()));
            $id = $this->connection->lastInsertId();
            $object->setId($id);
            return $object;

        }else throw new ObjectTypeException('object is not a User');
    }

    /**
     * @param User $object
     * @return bool
     */
    public function delete($object)
    {
        if($object instanceof User){
            $prepare = $this->connection->prepare('delete from user where id=?');
            return $prepare->execute(array($object->getId()));
        }else throw new ObjectTypeException('object is not a User');
    }

    /**
     * @param $id
     * @return User
     * @throws UserNotFoundException
     */
    public function findById($id)
    {
        $prepare = $this->connection->prepare('select * from user where id=?');
        $prepare->execute(array($id));
        if($prepare->rowCount()==1){
            $result = $prepare->fetch(PDO::FETCH_LAZY);
            $user = new User();
            $user->setId($result->id);
            $user->setEmail($result->email);
            $user->setPassword($result->password);
            $user->setPrivileged($result->privileged);
            $user->setActive($result->active);
            $user->setCreateAt($result->create_at);
            return $user;
        }else throw new UserNotFoundException('User not found');
    }

    /**
     * @param $page
     * @return array
     */
    public function findAll($page)
    {
        $prepare = $this->connection->prepare('select * from user');
        $prepare->execute();
        $all = $prepare->fetchAll();
        $array = array();
        foreach ($all as $result){
            $user = new User();
            $user->setId($result['id']);
            $user->setEmail($result['email']);
            $user->setPassword($result['password']);
            $user->setPrivileged($result['privileged']);
            $user->setActive($result['active']);
            $user->setCreateAt($result['create_at']);
            array_push($array,$user);
        }
        return $array;
    }

    /**
     * @param $email
     * @return User
     * @throws UserNotFoundException
     */
    public function findByEmail($email){
        $prepare = $this->connection->prepare('select * from user where email=? limit 1');
        $prepare->execute(array($email));
        if($prepare->rowCount()!=0){
            $result = $prepare->fetch(PDO::FETCH_LAZY);
            $user = new User();
            $user->setId($result->id);
            $user->setEmail($result->email);
            $user->setPassword($result->password);
            $user->setPrivileged($result->privileged);
            $user->setActive($result->active);
            $user->setCreateAt($result->create_at);
            return $user;
        }else throw new UserNotFoundException('User not found');
    }

    /**
     * @param $email
     * @return bool
     */
    public function hasEmail($email){
        $prepare = $this->connection->prepare('select * from user where email=? limit 1');
        $prepare->execute(array($email));
        if($prepare->rowCount()!=0){
            return true;
        }else return false;
    }
    public function hasId($id){
        $prepare = $this->connection->prepare('select * from user where id=? limit 1');
        $prepare->execute(array($id));
        if($prepare->rowCount()!=0){
            return true;
        }else return false;
    }

    public function getPasswordById($id){
        $prepare = $this->connection->prepare('select password from user where id=? limit 1');
        $prepare->execute(array($id));
        return $prepare->fetchColumn();
    }

    public function getRoleById($id){
        $prepare = $this->connection->prepare('select privileged from user where id=? limit 1');
        $prepare->execute(array($id));
        return $prepare->fetchColumn();
    }
}