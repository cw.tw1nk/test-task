<?php



namespace App\DAO;


use App\Exceptions\EmailConfirmationException;
use App\Models\EmailConfirmation;
use PDO;
use Respect\Validation\Exceptions\ObjectTypeException;

class EmailConfirmationDAO extends BaseDAO implements DAO
{


    public function save($object)
    {
        if ($object instanceof EmailConfirmation) {
            $id = $object->getId();
            if (isset($id)) {
                $prepare = $this->connection->prepare('select id, hash, user_id from email_confirmation where id=?;');
                $prepare->execute(array($id));
                if ($prepare->rowCount() == 1) {
                    $prepare = $this->connection->prepare('update email_confirmation set email=?, hash=?, user_id=? where id = ?');
                } else {
                    $prepare = $this->connection->prepare('insert into email_confirmation set  id = ?, hash=?, user_id=?');
                }
                $prepare->execute(array($object->getHash(), $object->getUserId(), $id));
                return $object;
            } else {
                $prepare = $this->connection->prepare('insert into email_confirmation set hash=?, user_id=?');
                $prepare->execute(array($object->getHash(), $object->getUserId()));
                $object->setId($this->connection->lastInsertId());
            }
            return $object;
        } else throw new ObjectTypeException('object is not a User');
    }

    public function delete($object)
    {
        if ($object instanceof EmailConfirmation) {
            $prepare = $this->connection->prepare('delete from email_confirmation where id=?');
            $prepare->execute(array($object->getId()));
        } else throw new ObjectTypeException('object is not a User');
    }

    public function findById($id)
    {
        $prepare = $this->connection->prepare('select id, hash, user_id from email_confirmation where id=?');
        $prepare->execute(array($id));
        if ($prepare->rowCount() == 1) {
            $result = $prepare->fetch(PDO::FETCH_LAZY);
            $confirmation = new EmailConfirmation();
            $confirmation->setId($result->id);
            $confirmation->setHash($result->hash);
            $confirmation->setUserId($result->user_id);
            return $confirmation;
        } else throw new EmailConfirmationException('User not found');
    }

    public function findAll($page)
    {
        $prepare = $this->connection->prepare('select id, hash, user_id from email_confirmation');
        $prepare->execute();
        $all = $prepare->fetchAll();
        $array = array();
        foreach ($all as $result) {
            $confirmation = new EmailConfirmation();
            $confirmation->setId($result['id']);
            $confirmation->setHash($result['hash']);
            $confirmation->setUserId($result['user_id']);
            array_push($array, $confirmation);
        }
        return $array;
    }

    public function findByEmail($email)
    {
        $prepare = $this->connection->prepare('select ec.id, ec.hash, ec.user_id from email_confirmation as ec inner join user u on ec.user_id = u.id where u.email = ? limit 1');
        $prepare->execute(array($email));
        if ($prepare->rowCount() == 1) {
            $result = $prepare->fetch(PDO::FETCH_LAZY);
            $confirmation = new EmailConfirmation();
            $confirmation->setId($result->id);
            $confirmation->setHash($result->hash);
            $confirmation->setUserId($result->user_id);
            return $confirmation;
        } else throw new EmailConfirmationException('User not found');
    }

    public function findByEmailAndHash($email, $hash)
    {
        $prepare = $this->connection->prepare('select ec.id, ec.hash, ec.user_id from email_confirmation as ec inner join user u on ec.user_id = u.id where u.email = ? and ec.hash = ? limit 1');
        $prepare->execute(array($email,$hash));
        if ($prepare->rowCount() == 1) {
            $result = $prepare->fetch(PDO::FETCH_LAZY);
            $confirmation = new EmailConfirmation();
            $confirmation->setId($result->id);
            $confirmation->setHash($result->hash);
            $confirmation->setUserId($result->user_id);
            return $confirmation;
        } else throw new EmailConfirmationException('User not found');
    }
    public function hasByEmailAndHash($email, $hash)
    {
        $prepare = $this->connection->prepare('select ec.id from email_confirmation as ec inner join user u on ec.user_id = u.id where u.email = ? and ec.hash = ? limit 1');
        $prepare->execute(array($email,$hash));
        if ($prepare->rowCount() !=0) {
            return true;
        } return false;
    }

}