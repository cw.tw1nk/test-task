<?php


namespace App\DAO;



use App\Exceptions\UserNotFoundException;
use App\Models\Session;
use PDO;
use Respect\Validation\Exceptions\ObjectTypeException;

class SessionDao extends BaseDAO implements DAO
{

    public function save($object)
    {
        if($object instanceof Session){
            $id = $object->getId();
            $session = $object->getSession();
            if (isset($id) or isset($session)){
                $prepare = $this->connection->prepare('select id from session where id=? or session=?');
                $prepare->execute(array($id, $session));
                if ($prepare->rowCount()==1) {
                    $object->setId($prepare->fetchColumn());
                    $prepare = $this->connection->prepare('update session set user_agent=?, ip=?, time=?, referrer = ?, session = ? where referrer = ?');
                    $prepare->execute(array($object->getUserAgent(), $object->getIp(), $object->getTime(), $object->getReferrer(), $object->getSession(), $id));
                    return $object;
                }
            }
            $prepare = $this->connection->prepare('insert into session set user_agent=?, ip=?, time=?, referrer=?, session=?');
            $prepare->execute(array($object->getUserAgent(), $object->getIp(), $object->getTime(), $object->getReferrer(), $object->getSession()));
            $id = $this->connection->lastInsertId();
            $object->setId($id);
            return $object;

        }else throw new ObjectTypeException('object is not a AuthSession');
    }

    public function delete($object)
    {
        if($object instanceof Session){
            $prepare = $this->connection->prepare('delete from session where id=?');
            return $prepare->execute(array($object->getId()));
        }else throw new ObjectTypeException('object is not a AuthSession');
    }


    public function findById($id)
    {
        $prepare = $this->connection->prepare('select * from session where id=?');
        $prepare->execute(array($id));
        if($prepare->rowCount()==1){
            $result = $prepare->fetch(PDO::FETCH_LAZY);
            $session = new Session();
            $session->setId($result->id);
            $session->setUserAgent($result->user_agent);
            $session->setIp($result->ip);
            $session->setTime($result->time);
            $session->setReferrer($result->referrer);
            $session->setSession($result->session);
            return $session;
        }else throw new UserNotFoundException('User not found');
    }

    public function findAll($page)
    {
        $prepare = $this->connection->prepare('select * from session');
        $prepare->execute();
        $all = $prepare->fetchAll();
        $array = array();
        foreach ($all as $result){
            $session = new Session();
            $session->setId($result['id']);
            $session->setUserAgent($result['user_agent']);
            $session->setIp($result['ip']);
            $session->setTime($result['time']);
            $session->setReferrer($result['referrer']);
            $session->setSession($result['session']);
            array_push($array,$session);
        }
        return $array;
    }

    public function hasByIdAndSession($id, $session_id){
        $prepare = $this->connection->prepare('select id from session where id = :id and session.session = :sesId');
        $prepare->execute(array('id'=>$id, 'sesId'=>$session_id));
        if ($prepare->rowCount() == 1){
            return true;
        }
        return false;
    }
}