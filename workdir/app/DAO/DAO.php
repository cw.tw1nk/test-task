<?php


namespace App\DAO;


interface DAO
{
    public function save($object);
    public function delete($object);
    public function findById($id);
    public function findAll($page);

}