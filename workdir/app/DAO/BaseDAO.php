<?php


namespace App\DAO;


use Interop\Container\ContainerInterface;
use PDO;

class BaseDAO
{
    protected $connection;

    /**
     * BaseDAO constructor.
     * @param $container
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

}