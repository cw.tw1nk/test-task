<?php



namespace App\DAO;


use App\DAO\Exception\EmailConfirmationException;
use App\Exceptions\TokenNotFoundException;
use App\Models\EmailConfirmation;
use App\Models\ResetPassword;
use PDO;
use Respect\Validation\Exceptions\ObjectTypeException;

class ResetPasswordDao extends BaseDAO implements DAO
{


    public function save($object)
    {
        if ($object instanceof ResetPassword) {
            $id = $object->getId();
            if (isset($id)) {
                $prepare = $this->connection->prepare('select id, token, user_id from reset_password where id=?;');
                $prepare->execute(array($id));
                if ($prepare->rowCount() == 1) {
                    $prepare = $this->connection->prepare('update reset_password set token=?, user_id=? where id = ?');
                } else {
                    $prepare = $this->connection->prepare('insert into reset_password set  id = ?, token=?, user_id=?');
                }
                $prepare->execute(array($object->getToken(), $object->getUserId(), $id));
                return $object;
            } else {
                $prepare = $this->connection->prepare('insert into reset_password set token=?, user_id=?');
                $prepare->execute(array($object->getToken(), $object->getUserId()));
                $object->setId($this->connection->lastInsertId());
            }
            return $object;
        } else throw new ObjectTypeException('object is not a ResetPassword');
    }

    public function delete($object)
    {
        if ($object instanceof ResetPassword) {
            $prepare = $this->connection->prepare('delete from reset_password where id=?');
            $prepare->execute(array($object->getId()));
        } else throw new ObjectTypeException('object is not a ResetPassword');
    }

    /**
     * @param $id
     * @return EmailConfirmation
     * @throws TokenNotFoundException
     */
    public function findById($id)
    {
        $prepare = $this->connection->prepare('select id, token, user_id from reset_password where id=?');
        $prepare->execute(array($id));
        if ($prepare->rowCount() == 1) {
            $result = $prepare->fetch(PDO::FETCH_LAZY);
            $confirmation = new EmailConfirmation();
            $confirmation->setId($result->id);
            $confirmation->setToken($result->token);
            $confirmation->setUserId($result->user_id);
            return $confirmation;
        } else throw new TokenNotFoundException('User not found');
    }

    public function findAll($page)
    {
        $prepare = $this->connection->prepare('select id, token, user_id from reset_password');
        $prepare->execute();
        $all = $prepare->fetchAll();
        $array = array();
        foreach ($all as $result) {
            $confirmation = new ResetPassword();
            $confirmation->setId($result['id']);
            $confirmation->setToken($result['token']);
            $confirmation->setUserId($result['user_id']);
            array_push($array, $confirmation);
        }
        return $array;
    }

    /**
     * @param $email
     * @return array
     */
    public function findByEmail($email)
    {
        $prepare = $this->connection->prepare('select ec.id, ec.token, ec.user_id from reset_password as ec inner join user u on ec.user_id = u.id where u.email = ?');
        $prepare->execute(array($email));
        $all = $prepare->fetchAll();
        $array = array();
        foreach ($all as $result) {
            $confirmation = new ResetPassword();
            $confirmation->setId($result['id']);
            $confirmation->setToken($result['token']);
            $confirmation->setUserId($result['user_id']);
            array_push($array, $confirmation);
        }
        return $array;
    }

    /**
     * @param $email
     * @param $hash
     * @return ResetPassword
     * @throws TokenNotFoundException
     */
    public function findByEmailAndToken($email, $hash)
    {
        $prepare = $this->connection->prepare('select ec.id, ec.token, ec.user_id from reset_password as ec inner join user u on ec.user_id = u.id where u.email = ? and ec.token = ? limit 1');
        $prepare->execute(array($email,$hash));
        if ($prepare->rowCount() == 1) {
            $result = $prepare->fetch(PDO::FETCH_LAZY);
            $confirmation = new ResetPassword();
            $confirmation->setId($result->id);
            $confirmation->setToken($result->token);
            $confirmation->setUserId($result->user_id);
            return $confirmation;
        } else throw new TokenNotFoundException('Token not found');
    }
    public function hasByEmailAndHash($email, $hash)
    {
        $prepare = $this->connection->prepare('select ec.id from reset_password as ec inner join user u on ec.user_id = u.id where u.email = ? and ec.token = ? limit 1');
        $prepare->execute(array($email,$hash));
        if ($prepare->rowCount() !=0) {
            return true;
        } return false;
    }
    public function deleteByUser($user_id){
        $prepare = $this->connection->prepare('delete from reset_password where user_id=?');
        $prepare->execute([$user_id]);
    }

}