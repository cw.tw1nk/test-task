<?php


namespace App\DAO;


use App\DAO\Exception\UserNotFoundException;
use App\Models\AuthSession;
use PDO;
use Respect\Validation\Exceptions\ObjectTypeException;

class AuthSessionDao extends BaseDAO implements DAO
{

    public function save($object)
    {
        if($object instanceof AuthSession){
            $id = $object->getId();
            if (isset($id)){
                $prepare = $this->connection->prepare('select id from auth_session where id=?');
                $prepare->execute(array($id));
                if ($prepare->rowCount()==1) {
                    $prepare = $this->connection->prepare('update auth_session set user_id=?, session_id=?, token=? where id = ?');
                    $prepare->execute(array($object->getUserId(), $object->getSessionId(), $object->getToken(), $id));
                    return $object;
                }
            }
            $prepare = $this->connection->prepare('insert into auth_session set user_id=?, session_id=?, token=?');
            $prepare->execute(array($object->getUserId(), $object->getSessionId(), $object->getToken()));
            $id = $this->connection->lastInsertId();
            $object->setId($id);
            return $object;

        }else throw new ObjectTypeException('object is not a AuthSession');
    }

    public function delete($object)
    {
        if($object instanceof AuthSession){
            $prepare = $this->connection->prepare('delete from auth_session where id=?');
            return $prepare->execute(array($object->getId()));
        }else throw new ObjectTypeException('object is not a AuthSession');
    }


    public function findById($id)
    {
        $prepare = $this->connection->prepare('select * from auth_session where id=?');
        $prepare->execute(array($id));
        if($prepare->rowCount()==1){
            $result = $prepare->fetch(PDO::FETCH_LAZY);
            $authSession = new AuthSession();
            $authSession->setId($result->id);
            $authSession->setUserId($result->user_id);
            $authSession->setSessionId($result->session_id);
            $authSession->setToken($result->token);
            return $authSession;
        }else throw new UserNotFoundException('User not found');
    }


    public function findAll($page)
    {
        $prepare = $this->connection->prepare('select * from auth_session');
        $prepare->execute();
        $all = $prepare->fetchAll();
        $array = array();
        foreach ($all as $result){
            $authSession = new AuthSession();
            $authSession->setId($result['id']);
            $authSession->setUserId($result['user_id']);
            $authSession->setSessionId($result['session_id']);
            $authSession->setToken($result['token']);
            array_push($array,$authSession);
        }
        return $array;
    }
    public function findByToken($token)
    {
        $prepare = $this->connection->prepare('select * from auth_session where token=?');
        $prepare->execute(array($token));
        if($prepare->rowCount()==1){
            $result = $prepare->fetch(PDO::FETCH_LAZY);
            $authSession = new AuthSession();
            $authSession->setId($result->id);
            $authSession->setUserId($result->user_id);
            $authSession->setSessionId($result->session_id);
            $authSession->setToken($result->token);
            return $authSession;
        }else throw new UserNotFoundException('User not found');
    }

    public function hasByUserAndToken($user_id, $token)
    {
        $prepare = $this->connection->prepare('select id from auth_session where user_id=? and token=?');
        $prepare->execute(array($user_id, $token));
        if($prepare->rowCount()==1){
            return true;
        }else return false;
    }

    public function deleteByToken($token)
    {
        $prepare = $this->connection->prepare('delete from auth_session where token=?');
        return $prepare->execute(array($token));
    }

    public function deleteByUser($user_id){

        $prepare = $this->connection->prepare('delete from auth_session where user_id=?');
        $prepare->execute([$user_id]);

    }


}