<?php

use App\Middleware\GuestMiddleware;
use Slim\App;

session_start();

$composer  = require(__DIR__ . '/../vendor/autoload.php');

$app = new App(include_once '../config/Application.php');

require __DIR__ . '/../routes/Web.php';
require __DIR__ . '/container.php';

