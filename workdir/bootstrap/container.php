<?php
/** @noinspection PhpUndefinedMethodInspection */

use App\Controllers\Admin\GuestController;
use App\Controllers\Admin\UserController;
use App\Controllers\Auth\EmailVerificationController;
use App\Controllers\Auth\ForgotPasswordController;
use App\Controllers\Auth\LoginController;
use App\Controllers\Auth\LogoutController;
use App\Controllers\Auth\RegisterController;
use App\Controllers\HomeController;
use App\Controllers\ProfileController;
use App\Controllers\TestController;
use App\DAO\AuthSessionDao;
use App\DAO\EmailConfirmationDAO;
use App\DAO\ResetPasswordDao;
use App\DAO\SessionDao;
use App\DAO\UserDAO;
use App\Services\GuestService;
use App\Services\MailService;
use App\Services\UserService;
use App\Twig\CsrfTwigExtension;
use App\Twig\AuthTwigExtension;
use App\Twig\Validation;
use App\Twig\ValidationTwigExtension;
use Slim\Container;
use Slim\Csrf\Guard;
use Slim\Flash\Messages;
use Slim\Http\Environment;
use Slim\Http\Response;
use Slim\Http\Uri;
use Slim\Views\Twig;
use Slim\Views\TwigExtension;


$container = $app->getContainer();

$container['db'] = function ($c){
    $db = $c['settings']['db'];
    $pdo = new PDO('mysql:host=' . $db['hostname'] . ';dbname=' . $db['database'],
        $db['user'], $db['password']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $pdo;
};
$container['validation'] = function($c){
    return new Validation();
};
$container['csrf'] = function (Container $c) {
    $guard = new Guard();
    $guard->setPersistentTokenMode(true);
    $guard->setFailureCallable(function ($request, Response $response, $next) use ($c) {
        return $response->withStatus(419, 'Token\'s lifetime has expired');
    });
    return $guard;
};
$app->add($container->get('csrf'));
$container['view'] = function ($c){
    $basePath = __DIR__.'/../resources/views/';
    $view = new Twig([
        $basePath.'main/'.$c['settings']['template'],
        'admin'=> $basePath.'admin',
        'mail'=>$basePath.'mail'
    ],[
        'cache' => __DIR__.'/../resources/cache',
        'auto_reload' => true,
    ]);
    $router = $c->get('router');
    $uri = Uri::createFromEnvironment(new Environment($_SERVER));
    $view->addExtension(new ValidationTwigExtension($c['validation']));
    $view->addExtension(new CsrfTwigExtension($c['csrf']));
    $view->addExtension(new AuthTwigExtension($router,$uri));
    $view->addExtension(new TwigExtension($router, $uri));
    return $view;
};

$container['flash'] = function () {
    return new Messages();
};

/*
 * DAO
 */

$container['UserDAO'] = function ($container) {
    return new UserDAO($container['db']);
};
$container['EmailConfirmationDAO'] = function ($container) {
    return new EmailConfirmationDAO($container['db']);
};
$container['authSessionDAO'] = function ($container) {
    return new AuthSessionDao($container['db']);
};
$container['sessionDao'] = function ($container) {
    return new SessionDao($container['db']);
};
$container['resetPasswordDAO'] = function ($container) {
    return new ResetPasswordDao($container['db']);
};

/*
 * Services
 */

$container['mailService'] = function ($container) {
    return new MailService($container);
};
$container['userService'] = function ($container) {
    return new UserService($container);
};
$container['guestService'] = function ($container) {
    return new GuestService($container);
};

/*
 *Controllers
 */

$container['HomeController'] = function ($container) {
    return new HomeController($container);
};
$container['RegisterController'] = function ($container) {
    return new RegisterController($container);
};
$container['GuestController'] = function ($container) {
    return new GuestController($container);
};
$container['UserController'] = function ($container) {
    return new UserController($container);
};
$container['LoginController'] = function ($container) {
    return new LoginController($container);
};
$container['EmailVerificationController'] = function ($container) {
    return new EmailVerificationController($container);
};
$container['TestController'] = function ($container) {
    return new TestController($container);
};
$container['LogoutController'] = function ($container) {
    return new LogoutController($container);
};
$container['ForgotPasswordController'] = function ($container) {
    return new ForgotPasswordController($container);
};
$container['ProfileController'] = function ($container) {
    return new ProfileController($container);
};


/*
 *
 */

