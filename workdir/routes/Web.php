<?php

use App\Middleware\GuestAccessMiddleware;
use App\Middleware\RoleAdminAccessMiddleware;
use App\Middleware\AuthMiddleware;
use App\Middleware\GuestMiddleware;
use Slim\App;

$container = $app->getContainer();
$app->add(new GuestMiddleware($container));

//$app->get('/test', function ($request, $response, $args) {
//    $db = $this->get('db');
//    $query = $db->query('select * from user')->fetchAll();
//    return $response->withJson(['data'=>$query]);
//})->add(new RoleAdminMiddleware());

$app->get('/', 'HomeController:index')
    ->setName('home.get');


$app->get('/register[/]', 'RegisterController:register')
    ->add(new GuestAccessMiddleware())
    ->setName('register.get');
$app->post('/register[/]', 'RegisterController:registerApi')
    ->add(new GuestAccessMiddleware())
    ->setName('register.post');

$app->get('/confirm/{email}', 'EmailVerificationController:confirm')
    ->setName('confirm.get');

$app->get('/login[/]', 'LoginController:login')
    ->add(new GuestAccessMiddleware())
    ->setName('login.get');
$app->post('/login[/]', 'LoginController:auth')
    ->add(new GuestAccessMiddleware())
    ->setName('login.post');
$app->get('/logout', 'LogoutController:logout')
    ->setName('logout');

$app->get('/forgot-password[/]','ForgotPasswordController:index')
    ->setName('forgot.get');
$app->post('/forgot-password','ForgotPasswordController:forgot')
    ->setName('forgot.post');

$app->get('/reset-password[/]','ForgotPasswordController:resetPassword')
    ->setName('reset.get');
$app->post('/reset-password','ForgotPasswordController:resetPassword')
    ->setName('reset.post');


$app->get('/profile[/]', 'ProfileController:index')
    ->add(new AuthMiddleware($container))
    ->setName('profile');
$app->post('/profile[/]', 'ProfileController:update')
    ->add(new AuthMiddleware($container))
    ->setName('profile.post');


$app->group('/admin', function (App $app){
    $app->redirect('[/]', '/admin/users', 302)->setName('admin');
    $app->get('/users', 'UserController:index')
        ->setName('admin.users');
    $app->get('/guests', 'GuestController:index')
        ->setName('admin.guests');

    $app->group('/api', function (App $app){
        $app->get('/users', 'UserController:users')
            ->setName('admin.api.users.get');
        $app->delete('/user/','UserController:deleteUser')
            ->setName('admin.api.user.delete')
            ->setName('admin.api.users.delete');
        $app->post('/users/','UserController:createUser')
            ->setName('admin.api.user.post');
        $app->put('/user/{id}','UserController:updateUser')
            ->setName('admin.api.user.put');
        $app->get('/guests', 'GuestController:guests')
            ->setName('admin.api.guests.get');
    });
})->add(new RoleAdminAccessMiddleware($container));

