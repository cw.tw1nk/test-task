# cw.app
To start the container run the command  
`docker-compose up -d nginx`  

To run the script, set the write access to the _/workdir/resources/cache_  
`chmod -R 777 ./workdir/resources/cache`
